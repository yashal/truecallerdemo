package com.example.truecallerassignment_yash_bhardwaj.databinding;
import com.example.truecallerassignment_yash_bhardwaj.R;
import com.example.truecallerassignment_yash_bhardwaj.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityMainBindingImpl extends ActivityMainBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.root, 4);
        sViewsWithIds.put(R.id.appBarLayout, 5);
        sViewsWithIds.put(R.id.toolbar, 6);
        sViewsWithIds.put(R.id.textView, 7);
        sViewsWithIds.put(R.id.scrollView1, 8);
        sViewsWithIds.put(R.id.headline2, 9);
        sViewsWithIds.put(R.id.scrollView2, 10);
        sViewsWithIds.put(R.id.headline3, 11);
        sViewsWithIds.put(R.id.scrollView3, 12);
        sViewsWithIds.put(R.id.fetch_data, 13);
        sViewsWithIds.put(R.id.no_network, 14);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityMainBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 15, sIncludes, sViewsWithIds));
    }
    private ActivityMainBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.appbar.AppBarLayout) bindings[5]
            , (com.google.android.material.button.MaterialButton) bindings[13]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[14]
            , (android.widget.LinearLayout) bindings[4]
            , (android.widget.ScrollView) bindings[8]
            , (android.widget.ScrollView) bindings[10]
            , (android.widget.ScrollView) bindings[12]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[3]
            , (androidx.appcompat.widget.Toolbar) bindings[6]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.textView1.setTag(null);
        this.textView2.setTag(null);
        this.textView3.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.everyTenthChar == variableId) {
            setEveryTenthChar((java.lang.String) variable);
        }
        else if (BR.worldCount == variableId) {
            setWorldCount((java.lang.String) variable);
        }
        else if (BR.viewModel == variableId) {
            setViewModel((com.example.truecallerassignment_yash_bhardwaj.ui.viewmodel.MainActivityViewModel) variable);
        }
        else if (BR.tenthChar == variableId) {
            setTenthChar((java.lang.String) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setEveryTenthChar(@Nullable java.lang.String EveryTenthChar) {
        this.mEveryTenthChar = EveryTenthChar;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.everyTenthChar);
        super.requestRebind();
    }
    public void setWorldCount(@Nullable java.lang.String WorldCount) {
        this.mWorldCount = WorldCount;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.worldCount);
        super.requestRebind();
    }
    public void setViewModel(@Nullable com.example.truecallerassignment_yash_bhardwaj.ui.viewmodel.MainActivityViewModel ViewModel) {
        this.mViewModel = ViewModel;
    }
    public void setTenthChar(@Nullable java.lang.String TenthChar) {
        this.mTenthChar = TenthChar;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.tenthChar);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String everyTenthChar = mEveryTenthChar;
        java.lang.String worldCount = mWorldCount;
        java.lang.String tenthChar = mTenthChar;

        if ((dirtyFlags & 0x11L) != 0) {
        }
        if ((dirtyFlags & 0x12L) != 0) {
        }
        if ((dirtyFlags & 0x18L) != 0) {
        }
        // batch finished
        if ((dirtyFlags & 0x18L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView1, tenthChar);
        }
        if ((dirtyFlags & 0x11L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView2, everyTenthChar);
        }
        if ((dirtyFlags & 0x12L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView3, worldCount);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): everyTenthChar
        flag 1 (0x2L): worldCount
        flag 2 (0x3L): viewModel
        flag 3 (0x4L): tenthChar
        flag 4 (0x5L): null
    flag mapping end*/
    //end
}