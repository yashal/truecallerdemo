package com.example.truecallerassignment_yash_bhardwaj.ui.activity

import android.graphics.Color
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.getSystemService
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.truecallerassignment_yash_bhardwaj.R
import com.example.truecallerassignment_yash_bhardwaj.databinding.ActivityMainBinding
import com.example.truecallerassignment_yash_bhardwaj.ui.viewmodel.MainActivityViewModel
import com.example.truecallerassignment_yash_bhardwaj.utils.ConnectivityChecker
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var connectivityChecker: ConnectivityChecker
    private lateinit var model: MainActivityViewModel
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        model = ViewModelProvider(this).get(MainActivityViewModel::class.java)
        binding.viewModel = model

        supportActionBar?.title = "Data Display"
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar)
        val connectivityManager = this@MainActivity.getSystemService<ConnectivityManager>()
        if (connectivityManager != null) {
            connectivityChecker = ConnectivityChecker(connectivityManager)
        }
        fetch_data.setOnClickListener {
            checkConnection()
        }
        no_network.setOnClickListener {
            checkConnection()
        }
    }

    private fun checkConnection() {
        connectivityChecker.apply {
            lifecycle.addObserver(this)
            connectedStatus.observe(this@MainActivity, Observer<Boolean> {
                if (it) {
                    loadData()
                } else {
                    showError()
                }
            })
        }
    }

    private fun loadData() {
        root.visibility = View.VISIBLE
        no_network.visibility = View.GONE
        model.getTenthChar().observe(this, Observer<String> { value ->
            value?.let {
                binding.tenthChar =it
            }
        })
        model.getEveryTenthChar().observe(this, Observer<String> { value ->
            value?.let {
                binding.everyTenthChar = it
            }
        })
        model.getWordsCount().observe(this, Observer<String> { value ->
            value?.let {
                binding.worldCount = it
            }
        })
    }

    private fun showError() {
        root.visibility = View.GONE
        no_network.visibility = View.VISIBLE
    }
}
